**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 7 Particle Detectors**

Date: 14.06.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |
