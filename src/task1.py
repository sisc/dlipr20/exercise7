from __future__ import division
from comet_ml import Experiment
import numpy as np
import matplotlib.pyplot as plt
import famous
from tensorflow import keras
layers = keras.layers


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise7", workspace="EnterGroupWorkspaceHere")


"""
The dataset contains 29900 simulated cosmic ray air events detected with
a FAMOUS-61 telescope at the HAWC observatory (courtesy of Merlin Schaufel).
Note: This is a simplified dataset for demonstration purposes.

Your task is to train a classifier that identifies photons from a background of
proton air showers, based on the following features:
- particle  particle type: 0 = proton, 1 = photon
- pixels    shower image: measured photons for each of the 61 pixels
- logE      log10(E / eV) (measured by HAWC or FAMOUS)
- phi       direction phi [rad] (measured by HAWC)
- theta     direction theta [rad] (measured by HAWC)
- rob       impact point [m] (measured by HAWC)

Sub-tasks:
 a) Set up a classification network to discriminate photons from protons.
    The network should use convolutions to work on the shower image and merge in
    the remaining features at some points in the network.
 b) Investigate the number of photons and protons in the dataset.
    When fitting the model, set the class_weights to give equal importance to
    protons and photons.
 c) Train your network to at least 75% test accuracy.
 d) Investigate the distribution of photon scores.
    What are the precision and recall of your model?
    precision = true positives / (true positives + false positives)
    recall = true positives / (true positives + false negatives)
"""

# ---------------------------------------------------
# Load and prepare dataset - you can leave as is.
# ---------------------------------------------------
data = np.load('/net/scratch/deeplearning/FAMOUS/data.npz')

# shower image: number of detected photons for each pixel (axial coordinates)
S = np.array([famous.vector2matrix(m) for m in data['pixels']])
S = np.log10(S + 1) / 3

# total number of detected photons
logS = np.log10(np.sum(data['pixels'], axis=1))
logS -= logS.min()

# energy - log10(E / 10 TeV)
logE = data['logE'] - 13

# impact point
Rob = data['rob'] / 7000. - 1.5

# shower direction
xdir = np.cos(data['phi'])
ydir = np.sin(data['phi'])

# select features
X1 = S[..., np.newaxis]
X2 = np.stack([logE, Rob, xdir, ydir], axis=-1)

# target features / labels
y = data['particle']  # gamma = 1, proton = 0
Y = keras.utils.to_categorical(y, 2)

# hold out the last 3000 events as test set
X1_train, X1_test = np.split(X1, [-3000])
X2_train, X2_test = np.split(X2, [-3000])
Y_train, Y_test = np.split(Y, [-3000])


# ---------------------------------------------------
# Model & Training
# ---------------------------------------------------
input1 = layers.Input(shape=(9, 9, 1))  # shower image
input2 = layers.Input(shape=(4,))  # other features

# TODO: define a suitable network with a first convolution part working on the
# shower image, and the remaining features merged in later

output = layers.Dense(2, activation='softmax')(z)

model = keras.models.Model(inputs=[input1, input2], outputs=output)
print(model.summary())

model.compile(
    loss='categorical_crossentropy',
    optimizer=keras.optimizers.Adam(lr=1E-3),
    metrics=['accuracy'])

model.fit(
    [X1_train, X2_train],
    Y_train,
    class_weight={0: 1, 1: 1},  # set class weights to achieve a balanced matching of photons / protons
    ...)

model.save('model.h5')


# ---------------------------------------------------
# Evaluation
# ---------------------------------------------------
loss, acc = model.evaluate([X1_test, X2_test], Y_test)
print('Test loss %.3f, test accuracy %.3f' % (loss, acc))


# plot distribution of photon-scores
Yp = model.predict([X1_test, X2_test])
y_test = np.argmax(Y_test, axis=1)
s0 = Yp[~y_test.astype(bool), 1]  # predicted photon-score for true protons
s1 = Yp[y_test.astype(bool), 1]  # predicted photon-score for true photons
fig, ax = plt.subplots(1)
plt.hist(s0, label='true protons', bins=np.linspace(0, 1, 31), alpha=0.6, normed=True)
plt.hist(s1, label='true photons', bins=np.linspace(0, 1, 31), alpha=0.6, normed=True)
plt.axvline(0.5, color='r', linestyle='--', label='decision boundary')
plt.legend()
plt.grid()
plt.xlabel('Photon score')
plt.ylabel('$p$(score)')
plt.savefig('score-distribution.png', bbox_inches='tight')
experiment.log_figure(figure=plt)
