from __future__ import division
from comet_ml import Experiment
import matplotlib.pyplot as plt
import numpy as np
from tensorflow import keras
layers = keras.layers

# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise7", workspace="EnterGroupWorkspaceHere")

"""
The dataset contains 200,000 simulated cosmic ray air showers measured with a
surface detector of water Cherenkov tanks, such as the Pierre Auger Observatory.
The primary particles are protons with energies between 10^18.5 and 10^20 eV
and uniformly distributed arrival directions with a maximum zenith angle of 60 degrees.

Each detector station measures the following 2 quantities, which are stored in
form of a map (2D array) corresponding to the station positions in offset coordinates
- time: time point of detection in [s]
- signal: signal strength [arbitrary units]

The following shower properties need to be reconstructed
- showeraxis: x,y,z unit vector of the shower arrival direction
- showercore: position of the shower core in [m]
- logE: log10(energy / eV)

Your tasks are
 a) Complete the provided script to set up a multi-task regression network for
    simultaneously predicting all three shower properties. The network should
    consist of a part that is common to all three objectives, followed by an
    individual subnetwork / tower for each objective.
    As objectives you can use mean squared error.
 b) We want to train the network to the following precision (evaluation provided)
    - less than 1.5 degrees angular resolution
    - less than 25 m core position resolution
    - less than 10% relative energy uncertainty: (E_predicted - E_true) / E_true
    Estimate what these requirements mean in terms of mean squared error loss
    and adjust the relative weights of the objectives accordingly.
 c) Train your network to the above precision.
 d) Plot and interpret the training curve, both with and without the objective weights
"""

# ---------------------------------------------------
# Load and prepare dataset - you can leave as is.
# ---------------------------------------------------
data = np.load('/net/scratch/deeplearning/airshower/auger-shower-planar.npz')

# time map, values standard normalized with untriggered stations set to 0
T = data['time']
T -= np.nanmean(T)
T /= np.nanstd(T)
T[np.isnan(T)] = 0

# signal map, values normalized to range 0-1, untriggered stations set to 0
S = data['signal']
S = np.log10(S)
S -= np.nanmin(S)
S /= np.nanmax(S)
S[np.isnan(S)] = 0

# input features
X = np.stack([T, S], axis=-1)

# target features
# direction - x,y,z unit vector
y1 = data['showeraxis']

# core position - x,y [m]
y2 = data['showercore'][:, 0:2]
y2 /= 750

# energy - log10(E/eV) in range [18.5, 20]
y3 = data['logE']
y3 -= 19.25

# hold out the last 20000 events as test data
X_train, X_test = np.split(X, [-20000])
y1_train, y1_test = np.split(y1, [-20000])
y2_train, y2_test = np.split(y2, [-20000])
y3_train, y3_test = np.split(y3, [-20000])


# ----------------------------------------------------------------------
# Model & Training
# ----------------------------------------------------------------------
input1 = layers.Input(shape=(9, 9, 2))

# TODO: define a suitable network consisting of 2 parts:
# 1) a common network part (you can try a convolutional stack with ResNet- or
#    or DenseNet-like shortcuts)
#   z = ...
# 2) separate network parts for the individual objectives
#   z1 = ...
#   z2 = ...
#   z3 = ...

output1 = layers.Dense(3, name='direction')(z1)
output2 = layers.Dense(2, name='core')(z2)
output3 = layers.Dense(1, name='energy')(z3)

model = keras.models.Model(inputs=input1, outputs=[output1, output2, output3])

print(model.summary())

model.compile(
    loss=['mse', 'mse', 'mse'],
    loss_weights=[1, 1, 1],  # you can give more weight to individual objectives
    optimizer=keras.optimizers.Adam(lr=1E-3))

fit = model.fit(X_train, [y1_train, y2_train, y3_train], ...)

model.save('model-task1.h5')


# TODO: plot training history - with and without weights
# ...


# ----------------------------------------------------------------------
# Evaluation - this should work as is.
# ----------------------------------------------------------------------
losses = model.evaluate(X_test, [y1_test, y2_test, y3_test], batch_size=128, verbose=0)
print('Test loss')
print('%.5e (direction)' % losses[1])
print('%.5e (core)' % losses[2])
print('%.5e (energy)' % losses[3])
print('%.5e (sum)' % losses[0])

# predict output for test set and undo feature scaling
y1p, y2p, y3p = model.predict(X_test, batch_size=128)
y2_test *= 750  # core position
y3_test += 19.25  # energy
y2p *= 750
y3p += 19.25
y3p = y3p[:, 0]  # remove unnecessary last axis

# direction
d = np.sum(y1p * y1_test, axis=1) / np.sum(y1p**2, axis=1)**.5
d = np.arccos(np.clip(d, 0, 1)) * 180 / np.pi
reso = np.percentile(d, 68)
plt.figure()
plt.hist(d, bins=np.linspace(0, 3, 41))
plt.axvline(reso, color='C1')
plt.text(0.95, 0.95, '$\sigma_{68} = %.2f^\circ$' % reso, ha='right', va='top', transform=plt.gca().transAxes)
plt.xlabel(r'$\Delta \alpha$ [deg]')
plt.ylabel('#')
plt.grid()
plt.savefig('hist-direction.png', bbox_inches='tight')
experiment.log_figure(figure=plt)

# core position
d = np.sum((y2_test - y2p)**2, axis=1)**.5
reso = np.percentile(d, 68)
plt.figure()
plt.hist(d, bins=np.linspace(0, 40, 41))
plt.axvline(reso, color='C1')
plt.text(0.95, 0.95, '$\sigma_{68} = %.2f m$' % reso, ha='right', va='top', transform=plt.gca().transAxes)
plt.xlabel('$\Delta r$ [m]')
plt.ylabel('#')
plt.grid()
plt.savefig('hist-core.png', bbox_inches='tight')
experiment.log_figure(figure=plt)

# energy
d = 10**(y3p - y3_test) - 1
reso = np.std(d)
plt.figure()
plt.hist(d, bins=np.linspace(-0.3, 0.3, 41))
plt.xlabel('($E_\mathrm{rec} - E_\mathrm{true}) / E_\mathrm{true}$')
plt.ylabel('#')
plt.text(0.95, 0.95, '$\sigma = %.3f$' % reso, ha='right', va='top', transform=plt.gca().transAxes)
plt.grid()
plt.savefig('hist-energy.png', bbox_inches='tight')
experiment.log_figure(figure=plt)

plt.figure()
plt.scatter(y3_test, y3p)
plt.plot([18.5, 20], [18.5, 20], color='black')
plt.xlabel('$\log_{10}(E_\mathrm{true}/\mathrm{eV})$')
plt.ylabel('$\log_{10}(E_\mathrm{rec}/\mathrm{eV})$')
plt.grid()
plt.savefig('scat_energy.png', bbox_inches='tight')
experiment.log_figure(figure=plt)
